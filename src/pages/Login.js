import React, { useState, useEffect } from "react";
import axios from "axios";

const Signup = (props) => {
    const [state, setState] = useState({
        email: "",
        password: "",
        errors: [],
        loading: false,
    });

    function handleChange(event) {
        setState({
            ...state,
            [event.target.name]: event.target.value,
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        setState({ ...state, loading: true });
        const userData = {
            email: state.email,
            password: state.password,
        };
        axios
            .post("/login", userData)
            .then((response) => {
                localStorage.setItem(
                    "AuthToken",
                    `Bearer ${response.data.token}`,
                );
                setState({ ...state, loading: false });
            })
            .catch((error) => {
                setState({
                    ...state,
                    errors: error.response.data,
                    loading: false,
                });
            });
    }

    return (
        <div className="container mx-auto p-12">
            <h1 className="text-4xl">Log In</h1>
            <form className="relative" noValidate>
                <div className="flex items-center mt-4">
                    <label htmlFor="email" className="label flex-initial">
                        Email
                    </label>
                    <input
                        id="email"
                        type="text"
                        placeholder="Email"
                        name="email"
                        className="flex-1 ml-4 p-4 border border-gray-300 rounded-lg outline-none focus:shadow-inner"
                        onChange={(e) => handleChange(e)}
                    />
                </div>
                <div className="flex items-center mt-4">
                    <label htmlFor="password" className="label flex-initial">
                        Password
                    </label>
                    <input
                        id="password"
                        type="password"
                        placeholder="Password"
                        name="password"
                        className="flex-1 w-full ml-4 p-4 border border-gray-300 rounded-lg outline-none focus:shadow-inner"
                        onChange={(e) => handleChange(e)}
                    />
                </div>
                <button
                    type="submit"
                    onClick={(e) => handleSubmit(e)}
                    className="absolute right-0 m-4 mr-0 py-4 px-12 w-full md:w-auto rounded-lg bg-green-400 hover:bg-green-500 text-white font-semibold focus:outline-none focus:shadow-md hover:shadow-md"
                    disabled={state.loading}
                >
                    Log In
                </button>
            </form>
        </div>
    );
};

export default Signup;
