import React, { useState, useEffect } from "react";
import axios from "axios";
import Alert from "./Alert";

const Signup = () => {
    const [state, setState] = useState({
        email: "",
        password: "",
        errors: [],
        loading: false,
    });

    function handleChange(event) {
        setState({
            ...state,
            [event.target.name]: event.target.value,
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        setState({ ...state, loading: true });
        const userData = {
            email: state.email,
            password: state.password,
            confirmPassword: state.confirmPassword,
        };
        axios
            .post("/signup", userData)
            .then((response) => {
                localStorage.setItem("AuthToken", `${response.data.token}`);
                setState({ ...state, loading: false });
            })
            .catch((error) => {
                setState({
                    ...state,
                    errors: error.response.data,
                    loading: false,
                });
            });
    }

    return (
        <div className="relative container mx-auto p-12">
            {state.errors !== undefined && state.errors.length !== 0 && (
                <Alert errors={state.errors} />
            )}
            <h1 className="text-4xl">Sign Up</h1>
            <form method="post" className="relative">
                <div className="flex items-center mt-4">
                    <label htmlFor="email" className="label flex-initial">
                        Email
                    </label>
                    <input
                        id="email"
                        type="text"
                        placeholder="Email"
                        name="email"
                        className="flex-1 ml-4 p-4 border border-gray-300 rounded-lg outline-none focus:shadow-inner"
                        onChange={(e) => handleChange(e)}
                    />
                </div>
                <div className="flex items-center mt-4">
                    <label htmlFor="password" className="label flex-initial">
                        Password
                    </label>
                    <input
                        id="password"
                        type="password"
                        placeholder="Password"
                        name="password"
                        className="flex-1 w-full ml-4 p-4 border border-gray-300 rounded-lg outline-none focus:shadow-inner"
                        onChange={(e) => handleChange(e)}
                    />
                    <input
                        id="confirmPassword"
                        type="password"
                        placeholder="Confirm Password"
                        name="confirmPassword"
                        className="flex-1 w-full ml-4 p-4 border border-gray-300 rounded-lg outline-none focus:shadow-inner"
                        onChange={(e) => handleChange(e)}
                    />
                </div>
                <button
                    type="submit"
                    className="absolute right-0 m-4 mr-0 py-4 px-12 w-full md:w-auto rounded-lg bg-green-400 hover:bg-green-500 text-white font-semibold focus:outline-none focus:shadow-md hover:shadow-md"
                    onClick={(e) => handleSubmit(e)}
                >
                    Sign Up
                </button>
            </form>
        </div>
    );
};

export default Signup;
