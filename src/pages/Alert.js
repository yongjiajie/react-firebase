import React from "react";

const Alert = (errors) => {
    const err = errors.errors;
    return (
        <div className="fixed flex flex-no-wrap items-center justify-center w-full top-0 left-0 mt-8">
            <div className="p-4 bg-red-400 rounded-full font-semibold text-white">
                {err.email !== undefined && <p>Email: {err.email}</p>}
                {err.password !== undefined && (
                    <p>
                        Password: {err.password} {err.confirmPassword}
                    </p>
                )}
            </div>
        </div>
    );
};

export default Alert;
