# React with Firebase

This project was created to learn how to integrate Firebase with React. This acts as the "front-end" of our Firebase app, sending API calls, receiving responses, displaying them, and routing users to different routes.

The technologies used are:

- React (i.e. React hooks)
- `react-router-dom`
- `axios`

## Features

- Login to get authentication token
- Signup to create user in Firebase for authentication
